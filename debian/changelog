pyracerz (0.2+dfsg-4) unstable; urgency=medium

  * Team upload.

  [ Mike Small ]
  * Fix compatibility with Python 3.13 (Closes: #1092627)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 13 Jan 2025 11:30:09 +0100

pyracerz (0.2+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Tweak the watch file for weird upstream versioning: 0.11 -> 0.1.1

 -- Alexandre Detiste <tchet@debian.org>  Fri, 10 May 2024 14:32:52 +0200

pyracerz (0.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * drop extraneous dependency on SDL1 lib (Closes: #1038875)
  * add d/.gitignore
  * use new dh-sequence-python3
  * debian/*: move files for debhelper 14/15 compatibility

 -- Alexandre Detiste <tchet@debian.org>  Sun, 10 Dec 2023 21:52:25 +0100

pyracerz (0.2+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

  [ Reiner Herrmann ]
  * Repack upstream tarball to exclude non-free font.
  * Drop non-free fonts and use a cartoony free font from Debian.
    (Closes: #1028082)
  * Update d/watch to version 4 and add handling of dfsg suffix.
  * d/postinst: replace deprecated --force with --force-all.
  * Bump Standards-Version to 4.6.2.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 08 Jan 2023 19:04:08 +0100

pyracerz (0.2-9) unstable; urgency=medium

  * Team upload.
  * Port to Python 3. (Closes: #912499)
    - skip processing of private dirs with dh_python3,
      to prevent removal of empty data directory
  * Update to debhelper compat level 12.
  * Update Standards-Version to 4.4.0:
    - drop obsolete menu file
    - declare that d/rules does not require root
  * Point Vcs-* fields to salsa.
  * Switch to https URL in d/copyright.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 03 Aug 2019 21:15:56 +0200

pyracerz (0.2-8) unstable; urgency=medium

  * Team upload.
  * Drop examples file.
    Do not install pyracerz.conf to the doc directory but to
    /usr/share/games/pyracerz instead.
    Update postinst accordingly and do not rely on the existence of the
    documentation directory during postinst because this is a Policy violation.
    (Closes: #762020)
  * Declare compliance with Debian Policy 3.9.6.

 -- Markus Koschany <apo@gambaru.de>  Sat, 20 Sep 2014 20:25:05 +0200

pyracerz (0.2-7) unstable; urgency=medium

  * Team upload.
  * dirs: Create /var/games/pyracerz again and avoid making the package
    uninstallable on new installations.

 -- Markus Koschany <apo@gambaru.de>  Sun, 14 Sep 2014 15:45:04 +0200

pyracerz (0.2-6) unstable; urgency=medium

  * Team upload.

  [ Markus Koschany ]
  * wrap-and-sort -sa.
  * Use compat level 9 and require debhelper >= 9.
  * Switch to source format 3.0.
    - Drop quilt build-dependency.
    - Drop README.source. Obsolete.
  * debian/control:
    - Remove versioned build-dependencies. They are trivially satisfied.
    - Do not recommend python-psyco anymore. It is no longer available.
    - Remove superfluous ${shlibs:Depends} substvar.
    - Declare compliance with Debian Policy 3.9.5
  * Convert man page to pyracerz.6 manually. Drop docbook-to-man b-d.
  * Install pyracerz.6 with manpages file.
  * Switch to dh sequencer and simplify debian/rules.
  * Add install file.
  * Keep dirs file but create only /usr/games and the missing musics directory.
  * Drop links file. Do not link the executable to /usr/bin.
  * Build with --parallel and with python2 and drop obsolete
    python-support b-d.
  * Update pyracerz.desktop. Add comment in German and keywords.
  * Add longtitle to menu file.
  * Delete game.orig.diff patch. Not applied.
  * Rename all patch extensions to patch.
  * Rename replay.difff.patch to replay.patch.
  * Update debian/copyright to copyright format 1.0.
  * postrm: Use rm -r /var/games/pyracerz on purge and try to remove /var/games
    if it is empty.
  * Add fix-100%-cpu.patch.
    Add idle commands to menu.py and game.py modules to avoid that the game
    consumes 100% cpu time at any time. Comment out insufficient collision
    detection in game.py that also made the game sluggish and unresponsive.
    The collision detection and AI need a complete overhaul anyway.
    (Closes: #402388)

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org.

  [ Barry deFreese ]
  * postinst: Create pyracerz.conf file with 664 not 666 permissions.
    (Closes: #683668)

 -- Markus Koschany <apo@gambaru.de>  Fri, 12 Sep 2014 22:57:46 +0200

pyracerz (0.2-5) unstable; urgency=low

  [ Barry deFreese ]
  * Add myself to uploaders.
  * Move direct source changes to quilt patches.
  * Move docbook-to-man and quilt to build-depends-indep.
  * Bump Standards Version to 3.8.3.
    + Menu policy transition.

  [ Carlos Galisteo ]
  * python-numeric dependence removed
    (Windows-only dependence). (Closes: #478454)
  * Maintainer field changed. Joining Debian Games Team.
  * Uploaders field added

  [ Ansgar Burchardt ]
  * debian/control: Add Vcs-* field
  * debian/control: Remove Homepage semi-field from description

 -- Barry deFreese <bdefreese@debian.org>  Fri, 25 Sep 2009 15:16:57 -0400

pyracerz (0.2-4) unstable; urgency=low

  * New maintainer. (Closes: #384659)
  * Copyright years added.
  * Migrated to python-support.
  * Patches are now managed with quilt.
  * Added patch for disable sound when mixer is not initialized
    correctly. Thanks to Piotr Ozarowski <ozarow@gmail.com>. (Closes #391536)
  * Long description and upstream URL included in man page.

 -- Carlos Galisteo <cgalisteo@k-rolus.net>  Mon, 16 Oct 2006 08:17:07 +0000

pyracerz (0.2-3) unstable; urgency=low

  * Bump Standards-Version: 3.7.2.
  * Updated package to complain with new Python Policy. Closes: #380914
    Thanks to Piotr Ozarowski <ozarow@gmail.com>
    - Changed control file.
    - Added debian/pycompat file.
  * Fixed missed space before Homepage pseudo header (dev ref 6.2.4).
  * Fixed modules/misc.py to dont call stopSound function when you use
    --nosound option.
  * Added python-numeric Dependence.
  * Fixed problem to write replay files.
  * Fixed problem to write in highscores file. (changed postinst to call chmod
    666 on pyracerz.conf file that is in /var/games directory.)

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Mon, 28 Aug 2006 14:12:31 -0300

pyracerz (0.2-2) unstable; urgency=low

  * Removed debin/compat file and created DH_COMPAT var in debian/rules.
  * Updated to DH_COMPAT=5.
  * Change Architecture to all in control file. (closes: #356870)

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Tue, 14 Mar 2006 14:43:59 -0300

pyracerz (0.2-1) unstable; urgency=low

  * Initial release Closes: #334405

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Wed, 19 Oct 2005 14:41:01 -0200
